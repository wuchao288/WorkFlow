﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.Data.Enum
{
    public enum ConfirmLetterStatusEnum
    {
        [Description("保存")]
        Save = -1,
        [Description("申请")]
        Apply =0,
        [Description("通过")]
        Pass =1,
        [Description("驳回")]
        Reject =2
    }
}

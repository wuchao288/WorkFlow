﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WikeSoft.WorkFlowEngine.Enum;

namespace WikeSoft.WorkFlowEngine
{
    /// <summary>
    /// 消息实例
    /// </summary>
    public class FlowMessage
    {
        /// <summary>
        /// 状态码
        /// </summary>
        public CodeEum Code { get; set; }

        /// <summary>
        /// 消息
        /// </summary>
        public String Message { get; set; }

        public FlowMessage()
        {
            this.Code = CodeEum.Success;
            this.Message = "操作成功";
        }

        public FlowMessage(CodeEum code ,string message)
        {
            this.Code = code;
            this.Message = message;
        }
    }
}

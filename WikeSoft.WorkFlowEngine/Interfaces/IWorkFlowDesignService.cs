﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WikeSoft.WorkFlowEngine.Entities;
using WikeSoft.WorkFlowEngine.Filter;
using WikeSoft.WorkFlowEngine.Models;

namespace WikeSoft.WorkFlowEngine.Interfaces
{

    public interface IWorkFlowDesignService
    {
        /// <summary>
        /// 添加流程定义
        /// </summary>
        /// <param name="flowDef"></param>
        /// <returns>返回流程定义Id,flowDefId</returns>
        string AddFlowDef(Models.WorkFlowDefinition flowDef);

        /// <summary>
        /// 修改流程定义
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        FlowMessage UpdateFlowDef(Models.WorkFlowDefinition model);

        /// <summary>
        /// 删除流程定义
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        FlowMessage DeleteFlowDef(IList<string> ids);
        /// <summary>
        /// 得到流程定义对象
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        WorkFlowDefinition GetFlowDef(string id);
        /// <summary>
        /// 得到定义的流程
        /// </summary>
        /// <param name="flowDefId"></param>
        /// <returns></returns>
        WorkFlowModel GetFlowModel(string flowDefId);


        /// <summary>
        /// 保存流程设置
        /// </summary>
        /// <param name="flowDef"></param>
        /// <returns></returns>
        FlowMessage SaveDiagram(WorkFlowModel flowDef);


        FlowMessage SaveDiagramWithNewVersion(WorkFlowModel flowModel);

        /// <summary>
        /// 返回流程图
        /// </summary>
        /// <param name="flowDefId">流程定义Id</param>
        /// <returns></returns>
        Bitmap GetBitmap(string flowDefId);

        /// <summary>
        /// 分页查询流程定义
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        PagedResult<WorkFlowModel> GetList(FlowDefFilter filter);


    }
}

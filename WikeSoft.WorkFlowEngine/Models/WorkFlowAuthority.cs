﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.WorkFlowEngine.Models
{
    /// <summary>
    /// 节点权限
    /// </summary>
    public class WorkFlowAuthority
    {
        /// <summary>
        /// 角色名称
        /// </summary>
        public string RoleNames { get; set; }
        /// <summary>
        /// 角色
        /// </summary>
        public string RoleIds { get; set; }
        /// <summary>
        /// 用户名称
        /// </summary>
        public string UserNames { get; set; }
        /// <summary>
        /// 用户Ids
        /// </summary>
        public string UserIds { get; set; }
    }
}

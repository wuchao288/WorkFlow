﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.WorkFlowEngine.Models
{
    public class TaskLink
    {
        ///<summary>
        /// 连接线ID
        ///</summary>
        public string Id { get; set; } // NodelLinkId (Primary key)

        ///<summary>
        /// 流程定义Id
        ///</summary>
        public string FlowDefId { get; set; } // FlowDefId

        ///<summary>
        /// 连接线起点ID
        ///</summary>
        public string SourceId { get; set; } // SourceId

        ///<summary>
        /// 连接线，终点ID
        ///</summary>
        public string TargetId { get; set; } // TargetId

        ///<summary>
        /// 连接ID(如con_20)
        ///</summary>
        public string LinkId { get; set; } // LinkId (length: 50)

        ///<summary>
        /// 连接线类型（直线，折线）
        ///</summary>
        public string LinkType { get; set; } // LinkType (length: 50)

        ///<summary>
        /// 开始左边距
        ///</summary>
        public double StartLeft { get; set; } // StartLeft

        ///<summary>
        /// 开始上边距
        ///</summary>
        public double StartTop { get; set; } // StartTop

        ///<summary>
        /// 起点位置(RightMiddle, LeftMiddle, BottomCenter, TopCenter)
        ///</summary>
        public string StartPostion { get; set; } // StartPostion (length: 50)

        ///<summary>
        /// 终点左边距
        ///</summary>
        public double EndLeft { get; set; } // EndLeft

        ///<summary>
        /// 终点左边距
        ///</summary>
        public double EndTop { get; set; } // EndTop

        ///<summary>
        /// 终点位置 (RightMiddle, LeftMiddle, BottomCenter, TopCenter)
        ///</summary>
        public string EndPostion { get; set; } // EndPostion (length: 50)

        ///<summary>
        /// 画图路径
        ///</summary>
        public string Path { get; set; } // Path (length: 500)

        ///<summary>
        /// 连接线名称
        ///</summary>
        public string LinkName { get; set; } // LinkName (length: 50)

        ///<summary>
        /// 转出条件
        ///</summary>
        public string Condition { get; set; } // Condition (length: 100)

        ///<summary>
        /// 连接名称的位置（左边距）
        ///</summary>
        public double LinkNameLeft { get; set; } // LinkNameLeft

        ///<summary>
        /// 上边距
        ///</summary>
        public double LinkNameTop { get; set; } // LinkNameTop

    }
}

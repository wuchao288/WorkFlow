﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.WorkFlowEngine.Models
{
    public class WorkFlowModel
    {


        ///<summary>
        /// 流程定义Id
        ///</summary>
        public string Id { get; set; } // FlowDefId (Primary key)

        ///<summary>
        /// 流程定义Key值，必填
        ///</summary>
        public string WorkFlowDefKey { get; set; } // FlowDefKey (length: 100)

        ///<summary>
        /// 流程定义名称
        ///</summary>
        public string WorkFlowDefName { get; set; } // FlowDefName (length: 100)

        ///<summary>
        /// 创建时间
        ///</summary>
        public System.DateTime? CreateDate { get; set; } // CreateDate


        public string CategoryId { get; set; }

        [Display(Name = "所属分类")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        public string CategoryName { get; set; }

        public List<TaskDefinition> Nodes { get; set; }

        public List<TaskLink> NodeLinks { get; set; }
        public string Remark { get; set; }

        public WorkFlowModel()
        {
            Nodes  = new List<TaskDefinition>();
            NodeLinks = new List<TaskLink>();
        }
    }
}

﻿using WikeSoft.Core.Exception;

namespace WikeSoft.Core.Extension
{
    /// <summary>
    /// 异常扩展
    /// </summary>
    public static class ExceptionExtensions
    {
        /// <summary>
        /// 是否是提示类的异常
        /// </summary>
        /// <param name="exception"></param>
        /// <returns></returns>
        public static bool IsTipException(this System.Exception exception)
        {
            return exception is TipInfoException;
        }
    }
}

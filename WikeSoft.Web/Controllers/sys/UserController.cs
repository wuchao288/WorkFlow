﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WikeSoft.Core.Exception;
using WikeSoft.Enterprise.Enum;
using WikeSoft.Enterprise.Interfaces.Sys;
using WikeSoft.Enterprise.Models.Filters.Sys;
using WikeSoft.Enterprise.Models.Sys;

namespace WikeSoft.Web.Controllers.Sys
{
    /// <summary>
    /// User
    /// </summary>
    public class UserController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IRoleService _roleService;

        public UserController(IUserService userService, IRoleService roleService)
        {
            _userService = userService;
            _roleService = roleService;
        }


        public ActionResult FrameIndex()
        {
            return View();
        }





        /// <summary>
        /// 添加用户
        /// </summary>
        /// <returns></returns> 
        public ActionResult FrameAdd()
        {
            return View();
        }



        /// <summary>
        /// 添加用户
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult FrameAdd(UserAddModel model)
        {
            if (ModelState.IsValid)
            {
                var success = _userService.Add(model);
                if (success)
                    return Redirect("/User/FrameIndex?departmentId=" + model.DepartmentId);
            }
            return View(model);
        }

        /// <summary>
        /// 编辑用户
        /// </summary>
        /// <returns></returns> 
        public ActionResult FrameEdit(string id)
        {
            var model = _userService.Find(id);
            return View(model);
        }

        /// <summary>
        /// 编辑用户
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult FrameEdit(UserEditModel model)
        {
            if (ModelState.IsValid)
            {
                var success = _userService.Edit(model);
                if (success)
                    return Redirect("/User/FrameIndex?departmentId=" + model.DepartmentId);
            }
            return View(model);
        }


        // GET: User
        public ActionResult Index()
        {
            return View();
        }


        /// <summary>
        /// 添加用户
        /// </summary>
        /// <returns></returns> 
        public ActionResult Add()
        {
            return View();
        }



        /// <summary>
        /// 添加用户
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Add(UserAddModel model)
        {
            if (ModelState.IsValid)
            {
                var success = _userService.Add(model);
                if (success)
                    return RedirectToAction("Index");
            }
            return View(model);
        }

        /// <summary>
        /// 编辑用户
        /// </summary>
        /// <returns></returns> 
        public ActionResult Edit(string id)
        {
            var model = _userService.Find(id);
            return View(model);
        }

        /// <summary>
        /// 编辑用户
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(UserEditModel model)
        {
            if (ModelState.IsValid)
            {
                var success = _userService.Edit(model);
                if (success)
                    return RedirectToAction("Index");
            }
            return View(model);
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(IList<string> ids)
        {
            var success = _userService.Delete(ids);
            return success ? Ok() : Fail();
        }

        /// <summary>
        /// 判断用户名是否存在
        /// </summary>
        /// <returns></returns>
        [IgnorePasswordExpiration]
        public ActionResult IsExists(string Id, string UserName)
        {
            var exists = _userService.IsExists(Id, UserName);
            return JsonOk(!exists);
        }

        /// <summary>
        /// 判断员工编号是否存在
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userCode"></param>
        /// <returns></returns>
        [IgnorePasswordExpiration]
        public ActionResult IsExsitUserCode(string id, string userCode)
        {
            var exists = _userService.IsExsitUserCode(id, userCode);
            return JsonOk(!exists);
        }


        /// <summary>
        /// 验证密码是否是上一次的老密码
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [IgnorePasswordExpiration]
        public ActionResult IsOldPassword(ChangePasswordModel model)
        {
            var isOldPassword = _userService.IsOldPassword(User.Identity.GetLoginUserId(), model.Password);
            return JsonOk(!isOldPassword);
        }

        /// <summary>
        /// 验证旧密码是否输入正确
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [IgnorePasswordExpiration]
        public ActionResult IsPasswordCorrect(ChangePasswordModel model)
        {
            var isOldPassword = _userService.IsOldPassword(User.Identity.GetLoginUserId(), model.OldPassword);
            return JsonOk(isOldPassword);
        }

        /// <summary>
        /// 更改密码
        /// </summary>
        /// <returns></returns>
        [IgnorePasswordExpiration]
        public ActionResult ChangePassword()
        {

            var expirated = _userService.IsPasswordExpirated(User.Identity.GetLoginUserId());

            var user = _userService.Find(User.Identity.GetLoginUserId());
            ViewData["ExpiratedDate"] = user.PasswordExpirationDate;

            ViewData["IsPasswordExpirated"] = expirated;
            return View();
        }

        /// <summary>
        /// 更改密码
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [IgnorePasswordExpiration]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                model.UserId = User.Identity.GetLoginUserId();
                throw new TipInfoException("禁止修改密码");
                var success = _userService.ChangePassword(model);
                if (success)
                    ViewBag.Sucess = true;
                else
                {
                    ViewBag.Sucess = false;
                }
            }
            return View(model);
        }

        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetList(UserFilter filter)
        {
            var users = _userService.GetUsers(filter);
            return JsonOk(users);
        }

        /// <summary>
        /// 加载角色部分视图
        /// </summary>
        /// <param name="id">用户Id</param>
        /// <returns></returns>
        [ChildActionOnly]
        public PartialViewResult RolePartial(string id)
        {
            var roles = _roleService.GetAllRoles(id, RoleType.User);
            return PartialView("RolePartial", roles);
        }

        public ActionResult GetUsersByDepartment(List<string> departments)
        {
            List<UserModel> datas;
            if (departments != null)
            {
                datas = _userService.GetUsersByDepartment(departments);
            }
            else
            {
                datas = _userService.GetSubUsers(User.Identity.GetLoginUserId());
            }
           

            var list = datas.Select(x => new {x.Id,x.TrueName});

            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}
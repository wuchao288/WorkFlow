﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WikeSoft.Core.Extension;

using WikeSoft.Data.Models;
using WikeSoft.Data.Models.Filters;

using WikeSoft.Data.Models.Sys;
using WikeSoft.Enterprise.Interfaces.Sys;
using WikeSoft.Enterprise.Models;
using WikeSoft.Enterprise.Models.Filters.Sys;
using WikeSoft.Enterprise.Models.Sys;

namespace WikeSoft.Web.Controllers.Sys
{
    public class DepartmentController : BaseController
    {

        private readonly IDepartmentService _departmentService;

        /// <summary>
        /// ctor
        /// </summary>
        public DepartmentController(IDepartmentService departmentService)
        {
            _departmentService = departmentService;
        }

        // GET: Page

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ListView()
        {
            return View();
        }


        


        /// <summary>
        /// 岗位查询
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult Query(DepartmentFilter filter)
        {
            var result = _departmentService.Query(filter);
            return JsonOk(result);
        }

        /// <summary>
        /// 根据父节点Id获取树
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetTrees(ZTreeFilter model)
        {
            var parentId = model.id.IsBlank() ? string.Empty : model.id;
            var pages = _departmentService.GetByParentId(parentId);
            return JsonOk(pages);
        }


        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="id">父页面ID</param>
        /// <returns></returns>

        public ActionResult Add(string id)
        {
            DepartmentEditModel parent = null;
            if (id.IsNotBlank())
            {
                parent = _departmentService.Find(id);
            }
            var model = new DepartmentAddModel();
            if (parent != null)
            {
                model.ParentId = parent.Id;
                model.ParentName = parent.DepartmentName;
            }
           
            return View(model);
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Add(DepartmentAddModel model)
        {
            if (ModelState.IsValid)
            {
                var success = _departmentService.Add(model);
                if (success)
                {
                    return PartialView("CloseLayerPartial");
                }
            }
            return View(model);
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        public ActionResult Edit(string id)
        {
            var model = _departmentService.Find(id);
         
            return View(model);
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(DepartmentEditModel model)
        {
            if (ModelState.IsValid)
            {
                var success = _departmentService.Edit(model);
                if (success)
                {
                    return PartialView("CloseLayerPartial");
                }
                return PartialView("CloseLayerPartial");
            }
            return View(model);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id">主键ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(string id)
        {
            var success = _departmentService.Delete(id);
            return Ok(success);
        }




        public JsonResult GetData()
        {

            List<DepartmentModel> list = _departmentService.GetList();


            //var result = list.Select(c => new
            //{
            //    Id=c.Id.ToString(),
            //    Name = c.DepartmentName,
            //    Desc =c.Remark,
            //    ParentId = c.ParentId==Guid.Empty?"":c.ParentId.ToString()
            //}).ToList();
          
            return Json(list, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetMyDepartmentList()
        {
            string userId = User.Identity.GetLoginUserId();

            List<DepartmentModel> list = _departmentService.GetMyDepartmentList(userId);
            List < ZTreeModel > data =   list.Select(c => new ZTreeModel()
            {
                id = c.Id ?? string.Empty,
                pId = c.ParentId,
                name= c.DepartmentName,
                isParent = list.Exists(x=>x.ParentId== (c.Id ?? string.Empty)),
                open=true,
            }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
            //List<ZTreeModel>  _departmentService
        }


    }
}
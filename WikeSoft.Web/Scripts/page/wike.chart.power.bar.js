﻿$(function() {
    $.get("/PowerDecision/GetStatBarDatas", function(response) {
        var total = response.jsons;
        // 路径配置
        require.config({
            paths: {
                echarts: '/Scripts/echarts.2.2.7/build/dist'
            }
        });

        // 使用
        require(
            [
                'echarts',
                'echarts/chart/bar', // 使用柱状图就加载bar模块，按需加载
                'echarts/chart/pie' // 使用柱状图就加载bar模块，按需加载
            ],
            function(ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('barMain'));
                option = {
                    title: {
                        text: '柱状图分析'
                    },
                    tooltip: {
                        trigger: 'axis',
                        formatter: function(parms) {
                            var res = parms[0].name;
                            res += '<br>' + parms[0].seriesName + ':' + parms[0].value;
                            res += '<br>' + parms[1].seriesName + ':' + parms[1].value;
                            if (parms[0].value != 0) {
                                res += '<br>问题数占比：' + ((parms[1].value / parms[0].value) * 100).toFixed(2) + '%';
                            }
                            //alert(parms)
                            return res;
                        }
                    },
                    legend: {
                        data: ['行权数', '问题数']
                    },

                    calculable: true,
                    xAxis: [
                        {
                            type: 'category',
                            data: response.datas
                        }
                    ],
                    yAxis: [
                        {
                            type: 'value'
                        }
                    ],
                    series: [
                        {
                            name: '行权数',
                            type: 'bar',


                            itemStyle: {
                                normal: {
                                    label: {
                                        show: true,
                                        formatter: '{c}',
                                        textStyle: {
                                            fontSize: 16
                                        }
                                    },
                                    labelLine: { show: true }
                                }
                            },
                            data: response.totals

                        },
                        {
                            name: '问题数',
                            type: 'bar',
                            itemStyle: {
                                normal: {
                                    label: {
                                        show: true,
                                        formatter: function(parms, tick, callback) {
                                            var index = 0;
                                            for (var i = 0; i < total.length; i++) {
                                                if (parms.name == total[i].name) {
                                                    index = i;
                                                    break;
                                                }
                                            }

                                            //alert(parms.value + ":" + total[index]);
                                            if (total[index].value == 0) {
                                                return "";
                                            } else {
                                                var per = parms.value / total[index].value;
                                                return parms.value + '（' + (per * 100).toFixed(2) + '%）';
                                            }

                                        },
                                        textStyle: {
                                            fontSize: 16
                                        }
                                    },
                                    labelLine: { show: true }
                                }
                            },
                            data: response.problems

                        }
                    ]
                };
                // 为echarts对象加载数据
                myChart.setOption(option);
            }
        );
    });
});
﻿function openAddPage(companyId) {
   
    top.layer.open({
        title: '证书添加',
        type: 2,
        content: "/CompanyCertificate/Add?companyId="+companyId,
        area: ['1000px', '550px']
    });
}

function openEditPage() {
    var row = WikeGrid.GetData();
    if (row != null) {
        top.layer.open({
            title: '证书修改',
            type: 2,
            content: "/CompanyCertificate/Edit?id=" + row.Id,
            area: ['1000px', '550px']
        });
    } else {
        top.layer.alert("请选择要编辑的数据");
    }
}

function deleteData() {
    var delDatas = WikeGrid.GetDataTableDeleteData();
    if (delDatas.Len > 0) {
        var btn = $("#btnDelete");
        top.layer.confirm("确认要删除这" + delDatas.Len + "条数据？", {
            btn: ['确认', '取消'] //按钮
        }, function () {
            var url = '/CompanyCertificate/Delete';
            btn.button('loading');
            $.ajax({
                url: url,
                type: "POST",
                dataType: "json",
                data: JSON.stringify({ ids: delDatas.Data }),
                contentType: "application/json, charset=utf-8",
                success: function (data) {
                    btn.button('reset');
                    if (data.success) {
                        top.layer.alert("删除成功");
                        $("#table_list").trigger("reloadGrid");
                    } else {
                        top.layer.alert("删除失败：" + data.message);
                    }
                }
            });
        }, function () {
            btn.button('reset');
        });
    } else {
        top.layer.alert("请选择要删除的数据！");
    }
}